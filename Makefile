### gmake options (see make_libdb400.sh)
### - gmake -f Makefile
### - gmake -f Makefile TGT64=64
### gcc options (Makefile)
### -v            - verbose compile
### -Wl,-bnoquiet - verbose linker
### -shared       - shared object
### -maix64       - 64bit
### -isystem      - compile PASE system headers
### -nostdlib     - remove libgcc_s.a and crtcxa_s.o
CC          = gcc
# CCFLAGS32   = -v verbose
CCFLAGS32   = -g -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast
CCFLAGS64   = $(CCFLAGS32) -maix64 -DTGT64
AR          = ar
AROPT       = -X32_64
INCLUDEPATH = -isystem /QOpenSys/usr/include -I.

### SYSTEM400
LIBDEP400           = -L. -ldb400 -L/QOpenSys/usr/lib -nostdlib -lpthreads -lc -liconv -ldl -lpthread
SYSTEM40032         = system400
SYSTEM400LIBOBJS32  = system400.o
SYSTEM400LIBDEPS32  = /QOpenSys/usr/lib/crt0.o $(LIBDEP400)

### global
CCFLAGS          = $(CCFLAGS32)
SYSTEM400        = $(SYSTEM40032)
SYSTEM400LIBOBJS = $(SYSTEM400LIBOBJS32)
SYSTEM400LIBDEPS = $(SYSTEM400LIBDEPS32)

### tells make all things to do (ordered)
all: clean removeo $(SYSTEM400) install

### PASE
### generic rules
### (note: .c.o compiles all c parts in OBJS list)
.SUFFIXES: .o .c
.c.o:
	$(CC) $(CCFLAGS) $(INCLUDEPATH) -c $<

### -- SYSTEM400
$(SYSTEM40032): $(SYSTEM400LIBOBJS)
	$(CC) $(CCFLAGS) $(SYSTEM400LIBOBJS) $(SYSTEM400LIBDEPS) -o $(SYSTEM400)

### -- oh housekeeping?
clean:
	rm -f $(SYSTEM400)
removeo:
	rm -f *.o
install:
	cp ./system400 /QOpenSys/usr/bin/.

