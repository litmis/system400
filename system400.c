#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <sqlcli1.h>
#include <iconv.h>

#define SYS400_VERSION "1.0.11"

#define SYS400_EBCDIC_CCSID 37

#define SYS400_MAX_CMD 5000
#define SYS400_MAX_BUF 5000000

#define JOBLOG400_MAX_COLS 15
#define JOBLOG400_MAX_SIZE 200
#define JOBLOG400_MAX_REC 4

#define SQL_IS_INTEGER 0

#define DB2UTIL_MAX_ARGS 32
#define DB2UTIL_MAX_COLS 1024
#define DB2UTIL_MAX_ERR_MSG_LEN (SQL_MAX_MESSAGE_LENGTH + SQL_SQLSTATE_SIZE + 10)

#define DB2UTIL_EXPAND_CHAR 3
#define DB2UTIL_EXPAND_BINARY 2
#define DB2UTIL_EXPAND_OTHER 64
#define DB2UTIL_EXPAND_COL_NAME 128

void help(char *str);

int db2util_ccsid() {
  char * env_ccsid = getenv("CCSID");
  int ccsid = Qp2paseCCSID();
  if (env_ccsid) {
     ccsid = atoi(env_ccsid);
  }
  return ccsid;
}

void * db2util_new(int size) {
  void * buffer = malloc(size + 1);
  memset(buffer,0,size + 1);
  return buffer;
}

void db2util_free(char *buffer) {
  if (buffer) {
    free(buffer);
  }
}



int str_iconv(int isInput, char *fromBuffer, char *toBuffer, size_t sourceLen, size_t bufSize, int ile_ccsid, int pase_ccsid) {
  int i = 0;
  int rc = 0;
  char * ile_charset = NULL;
  char * pase_charset = NULL;
  iconv_t conv_to_pase = 0;
  iconv_t conv_to_ile = 0;
  char *source = fromBuffer;
  char *target = toBuffer;
  size_t sourceBytesLeft = sourceLen;
  size_t targetBytesLeft = bufSize;

  /* clear buffer */
  memset(toBuffer,0,bufSize);

  /* get/open convert */
  ile_charset = (char *) ccsidtocs(ile_ccsid);
  pase_charset = (char *) ccsidtocs(pase_ccsid);
  conv_to_pase = iconv_open(pase_charset, ile_charset);
  conv_to_ile = iconv_open(ile_charset, pase_charset);

  /* convert  */
  if (isInput) {
   rc = iconv(conv_to_ile, (char**)(&source), &sourceBytesLeft, &target, &targetBytesLeft);
  } else {
   rc = iconv(conv_to_pase, (char**)(&source), &sourceBytesLeft, &target, &targetBytesLeft);
  }

  /* close convert */
  (void)iconv_close(conv_to_pase);
  (void)iconv_close(conv_to_ile);

  return rc;
}

void str_replace(char *target, const char *needle, const char *replacement)
{
  char buffer[1024] = { 0 };
  char *insert_point = &buffer[0];
  const char *tmp = target;
  size_t needle_len = strlen(needle);
  size_t repl_len = strlen(replacement);
  while (1) {
    const char *p = strstr(tmp, needle);
    // walked past last occurrence of needle; copy remaining part
    if (p == NULL) {
      strcpy(insert_point, tmp);
      break;
    }
    // copy part before needle
    memcpy(insert_point, tmp, p - tmp);
    insert_point += p - tmp;
    // copy replacement string
    memcpy(insert_point, replacement, repl_len);
    insert_point += repl_len;
    // adjust pointers, move on
    tmp = p + needle_len;
  }
  // write altered string back to target
  strcpy(target, buffer);
}

SQLRETURN QCMDEXC_QSQSRVR(SQLHANDLE hdbc, char *sys_buff) {
  SQLRETURN sqlrc = SQL_SUCCESS;
  SQLRETURN sql_exec_rc = SQL_SUCCESS;
  SQLHANDLE hstmt = 0;
  SQLINTEGER sys_len = SYS400_MAX_CMD;
  SQLSMALLINT sql_data_type = 0;
  SQLUINTEGER sql_precision = 0;
  SQLSMALLINT sql_scale = 0;
  SQLSMALLINT sql_nullable = SQL_NO_NULLS;
  char cmd_buff[SYS400_MAX_CMD];
  SQLINTEGER cmd_len = SYS400_MAX_CMD;

  /* call */
  memset(cmd_buff,0,SYS400_MAX_CMD);
  sprintf(cmd_buff,"CALL QSYS2.QCMDEXC(?,?)");
  /* statement */
  sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, (SQLHDBC) hdbc, &hstmt);
  /* prepare */
  sqlrc = SQLPrepare((SQLHSTMT)hstmt, cmd_buff, (SQLINTEGER)SQL_NTS);
  /* string */
  sqlrc = SQLDescribeParam((SQLHSTMT)hstmt, 1, 
          &sql_data_type, &sql_precision, &sql_scale, &sql_nullable);
  sys_len = SQL_NTS;
  sqlrc = SQLBindParameter((SQLHSTMT)hstmt, 1,
          SQL_PARAM_INPUT, SQL_CHAR, sql_data_type,
          sql_precision, sql_scale, (SQLPOINTER)sys_buff, 0, &sys_len);
  /* length */
  sqlrc = SQLDescribeParam((SQLHSTMT)hstmt, 2, 
          &sql_data_type, &sql_precision, &sql_scale, &sql_nullable);
  cmd_len = strlen(sys_buff);
  sqlrc = SQLBindParameter((SQLHSTMT)hstmt, 2,
          SQL_PARAM_INPUT, SQL_C_LONG, sql_data_type,
          sql_precision, sql_scale, (SQLPOINTER)&cmd_len, 0, NULL);
  /* execute */
  sqlrc = SQLExecute((SQLHSTMT)hstmt);
  sql_exec_rc = sqlrc;
  /* printf("==>STRQSH -- %d return code\n",sqlrc); */
  /* close */
  sqlrc = SQLFreeHandle(SQL_HANDLE_STMT, hstmt);

 return sql_exec_rc;
}

SQLRETURN JOBLOG_QSQSRVR(SQLHANDLE hdbc) {
  /*
  *sort time descend (new entries first)
  */
  char * hostSql = "select\
  message_id as msgid,\
  message_type as msgtype,\
  message_subtype as msgsub,\
  severity as msgsev,\
  cast(message_timestamp as varchar(26)) as msgstamp,\
  to_library as msgtolib,\
  to_program as msgtopgm,\
  to_module as msgtomod,\
  cast(to_procedure as varchar(128)) as msgtoproc,\
  to_instruction as msgtoinst,\
  cast(message_text as varchar(200) ccsid 37) as msgtxt\
  from table(qsys2.joblog_info('*')) a\
  where severity >= 20 order by msgstamp DESC";
  int j = 0;
  int rec = 0;
  SQLRETURN sqlrc = SQL_SUCCESS;
  SQLRETURN sql_exec_rc = SQL_SUCCESS;
  SQLHANDLE hstmt = 0;
  SQLSMALLINT nResultCols = 0;
  SQLCHAR buff_name[JOBLOG400_MAX_COLS][JOBLOG400_MAX_SIZE];
  SQLCHAR buff_value[JOBLOG400_MAX_COLS][JOBLOG400_MAX_SIZE];
  SQLINTEGER buff_len[JOBLOG400_MAX_COLS];
  SQLSMALLINT buff_type[JOBLOG400_MAX_COLS];
  SQLINTEGER fStrLen[JOBLOG400_MAX_COLS];
  SQLSMALLINT name_length = 0;
  SQLSMALLINT type = 0;
  SQLINTEGER size = 0;
  SQLSMALLINT scale = 0;
  SQLSMALLINT nullable = 0;

  /* statement */
  sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, (SQLHDBC) hdbc, &hstmt);
  /* prepare */
  sqlrc = SQLPrepare((SQLHSTMT)hstmt, (SQLPOINTER)hostSql, (SQLINTEGER)SQL_NTS);
  /* execute */
  sqlrc = SQLExecute((SQLHSTMT)hstmt);
  sql_exec_rc = sqlrc;
  /* result set */
  sqlrc = SQLNumResultCols((SQLHSTMT)hstmt, &nResultCols);
  if (nResultCols > 0) {
    for (j = 0 ; j < nResultCols; j++) {
      size = JOBLOG400_MAX_SIZE;
      memset(buff_name[j],0,JOBLOG400_MAX_SIZE);
      memset(buff_value[j],0,JOBLOG400_MAX_SIZE);
      buff_type[j] = 0;
      fStrLen[j] = SQL_NTS;
      sqlrc = SQLDescribeCol((SQLHSTMT)hstmt, (SQLSMALLINT)(j + 1), (SQLCHAR *)buff_name[j], size, &name_length, &buff_type[j], &size, &scale, &nullable);
      sqlrc = SQLBindCol((SQLHSTMT)hstmt, (j + 1), SQL_CHAR, buff_value[j], size, &fStrLen[j]);
    }
    sqlrc = SQL_SUCCESS;
    while (sqlrc == SQL_SUCCESS) {
      sqlrc = SQLFetch(hstmt);
      if ( sqlrc == SQL_NO_DATA_FOUND || sqlrc < SQL_SUCCESS ) {
        break;
      }
      printf("-msgid %s",      buff_value[0]);
      printf(" -msgtype %s",   buff_value[1]);
      if (strlen(buff_value[2])) {
        printf(" -msgsub %s",    buff_value[2]);
      } else {
        printf(" -msgsub *NULL");
      }
      printf(" -msgsev %s",    buff_value[3]);
      printf(" -msgstamp %s",  buff_value[4]);
      printf(" -msgtolib %s",  buff_value[5]);
      printf(" -msgtopgm %s",  buff_value[6]);
      printf(" -msgtomod %s",  buff_value[7]);
      printf(" -msgtoproc %s", buff_value[8]);
      printf(" -msgtoinst %s", buff_value[9]);
      printf(" -msgtxt \"%s\"\n",  buff_value[10]);
      rec++;
      if (rec > JOBLOG400_MAX_REC) {
        break;
      }
    }
  } else {
    sql_exec_rc = -1;
  }
  /* close */
  sqlrc = SQLFreeHandle(SQL_HANDLE_STMT, hstmt);

 return sql_exec_rc;
}

SQLRETURN STRQSH_QSQSRVR(SQLHANDLE hdbc, int ccsid, int ebcdic, char * argv[]) {
  SQLRETURN sqlrc = SQL_SUCCESS;
  SQLRETURN sql_exec_rc = SQL_SUCCESS;
  char user_buff[SYS400_MAX_CMD];
  char sys_buff[SYS400_MAX_CMD];
  FILE *source = NULL;
  char ile_file[SYS400_MAX_CMD];
  char ile_buf[SYS400_MAX_BUF];
  SQLINTEGER ile_len = SYS400_MAX_BUF;
  char pase_buf[SYS400_MAX_BUF];
  SQLINTEGER pase_len = SYS400_MAX_BUF;

  /* system */
  memset(ile_file,0,SYS400_MAX_CMD);
  sprintf(ile_file,"/tmp/output%d.txt",getpid());
  memset(user_buff,0,SYS400_MAX_CMD);
  strcpy(user_buff,argv[1]);
  str_replace(user_buff, "'", "''");
  memset(sys_buff,0,SYS400_MAX_CMD);
  sprintf(sys_buff,"STRQSH CMD('/usr/bin/system -i \"%s\" > %s%s')",
    user_buff,argv[2],ile_file);
  /* printf("%s\n",sys_buff); */

  /* make the call */
  sql_exec_rc = QCMDEXC_QSQSRVR(hdbc, sys_buff);

  /* output to ascii */
  memset(ile_buf,0,ile_len);
  memset(pase_buf,0,pase_len);
  source = fopen(ile_file, "r");
  if (source) {
    ile_len = fread(ile_buf, 1, SYS400_MAX_BUF, source);
    fclose(source);
    sqlrc = str_iconv(0, ile_buf, pase_buf, ile_len, pase_len, ebcdic, ccsid);
    printf("%s\n",pase_buf);
  } else {
    printf("Error no output %s\n", ile_file);
    if (!sql_exec_rc) {
      sql_exec_rc = -1;
    }
  }
  /* remove file */
  unlink(ile_file);

  /* sqlrc */
  return sql_exec_rc;
}

SQLRETURN CMD_QSQSRVR(SQLHANDLE hdbc, char * argv[], int isOut) {
  SQLRETURN sqlrc = SQL_SUCCESS;
  SQLRETURN sql_exec_rc = SQL_SUCCESS;
  char sys_buff[SYS400_MAX_CMD];

  /* cmd */
  memset(sys_buff,0,SYS400_MAX_CMD);
  strcpy(sys_buff,argv[1]);

  /* make the call */
  sql_exec_rc = QCMDEXC_QSQSRVR(hdbc, sys_buff);

  /* error dump joblog (last entries) */
  if (isOut && sql_exec_rc != SQL_SUCCESS) {
    sqlrc = JOBLOG_QSQSRVR(hdbc);
  }

  /* sqlrc */
  return sql_exec_rc;
}

int db2util_check_sql_errors(SQLHANDLE handle, SQLSMALLINT hType, int rc)
{
  SQLCHAR msg[SQL_MAX_MESSAGE_LENGTH + 1];
  SQLCHAR sqlstate[SQL_SQLSTATE_SIZE + 1];
  SQLCHAR errMsg[DB2UTIL_MAX_ERR_MSG_LEN];
  SQLINTEGER sqlcode = 0;
  SQLSMALLINT length = 0;
  SQLCHAR *p = NULL;
  SQLSMALLINT recno = 1;
  if (rc == SQL_ERROR) {
    memset(msg, '\0', SQL_MAX_MESSAGE_LENGTH + 1);
    memset(sqlstate, '\0', SQL_SQLSTATE_SIZE + 1);
    memset(errMsg, '\0', DB2UTIL_MAX_ERR_MSG_LEN);
    if ( SQLGetDiagRec(hType, handle, recno, sqlstate, &sqlcode, msg, SQL_MAX_MESSAGE_LENGTH + 1, &length)  == SQL_SUCCESS ) {
      if (msg[length-1] == '\n') {
        p = &msg[length-1];
        *p = '\0';
      }
      printf("Error %s SQLCODE=%d\n", msg, sqlcode);
      return SQL_ERROR; 
    }
  }
  return SQL_SUCCESS;
}

int DB2_QSQSRVR(SQLHANDLE hdbc, char *argv[]) {
  int i = 0;
  int j = 0;
  int recs = 0;
  int rc = 0;
  char * iamspace = NULL;
  char * stmt_str = argv[1];
  char * stmt_parm = NULL;
  char * strip_value = NULL;
  int strip_done = 0;
  int len_value = 0;
  SQLHANDLE hstmt = 0;
  SQLSMALLINT nParms = 0;
  SQLSMALLINT nResultCols = 0;
  SQLSMALLINT name_length = 0;
  SQLCHAR *buff_name[DB2UTIL_MAX_COLS];
  SQLCHAR *buff_value[DB2UTIL_MAX_COLS];
  SQLINTEGER buff_len[DB2UTIL_MAX_COLS];
  SQLSMALLINT type = 0;
  SQLUINTEGER size = 0;
  SQLSMALLINT scale = 0;
  SQLSMALLINT nullable = 0;
  SQLINTEGER lob_loc = 0;
  SQLINTEGER loc_ind = 0;
  SQLSMALLINT loc_type = 0;
  SQLINTEGER fStrLen = SQL_NTS;
  SQLSMALLINT sql_data_type = 0;
  SQLUINTEGER sql_precision = 0;
  SQLSMALLINT sql_scale = 0;
  SQLSMALLINT sql_nullable = SQL_NO_NULLS;

  /* init */
  for (i=0;i < DB2UTIL_MAX_COLS;i++) {
    buff_name[i] = NULL;
    buff_value[i] = NULL;
    buff_len[i] = 0;
  }

  /* statement */
  rc = SQLAllocHandle(SQL_HANDLE_STMT, (SQLHDBC) hdbc, &hstmt);
  if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
    return SQL_ERROR;
  }
  /* prepare */
  rc = SQLPrepare((SQLHSTMT)hstmt, (SQLCHAR*)stmt_str, (SQLINTEGER)SQL_NTS);
  if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
    return SQL_ERROR;
  }
  /* number of input parms */
  rc = SQLNumParams((SQLHSTMT)hstmt, (SQLSMALLINT*)&nParms);
  if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
    return SQL_ERROR;
  }
  if (nParms > 0) {
    for (i = 0; i < nParms; i++) {
      stmt_parm = argv[i + 2];
      rc = SQLDescribeParam((SQLHSTMT)hstmt, (SQLUSMALLINT)(i + 1), 
             &sql_data_type, &sql_precision, &sql_scale, &sql_nullable);
      if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
        return SQL_ERROR;
      }
      buff_len[i] = SQL_NTS;
      rc = SQLBindParameter((SQLHSTMT)hstmt, (SQLUSMALLINT)(i + 1),
             SQL_PARAM_INPUT, SQL_CHAR, sql_data_type,
             sql_precision, sql_scale, stmt_parm, 0, &buff_len[i]);
      if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
        return SQL_ERROR;
      }
    }
  }
  /* execute */
  rc = SQLExecute((SQLHSTMT)hstmt);
  if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
    return SQL_ERROR;
  }
  /* result set */
  rc = SQLNumResultCols((SQLHSTMT)hstmt, &nResultCols);
  if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
    return SQL_ERROR;
  }
  if (nResultCols > 0) {
    for (i = 0 ; i < nResultCols; i++) {
      size = DB2UTIL_EXPAND_COL_NAME;
      buff_name[i] = db2util_new(size);
      buff_value[i] = NULL;
      rc = SQLDescribeCol((SQLHSTMT)hstmt, (SQLSMALLINT)(i + 1), (SQLCHAR *)buff_name[i], size, &name_length, &type, &size, &scale, &nullable);
      if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
        return SQL_ERROR;
      }
      /* dbcs expansion */
      switch (type) {
      case SQL_CHAR:
      case SQL_VARCHAR:
      case SQL_CLOB:
      case SQL_DBCLOB:
      case SQL_UTF8_CHAR:
      case SQL_WCHAR:
      case SQL_WVARCHAR:
      case SQL_GRAPHIC:
      case SQL_VARGRAPHIC:
      case SQL_XML:
        size = size * DB2UTIL_EXPAND_CHAR;
        buff_value[i] = db2util_new(size);
        rc = SQLBindCol((SQLHSTMT)hstmt, (i + 1), SQL_CHAR, buff_value[i], size, &fStrLen);
        break;
      case SQL_BINARY:
      case SQL_VARBINARY:
      case SQL_BLOB:
        size = size * DB2UTIL_EXPAND_BINARY;
        buff_value[i] = db2util_new(size);
        rc = SQLBindCol((SQLHSTMT)hstmt, (i + 1), SQL_CHAR, buff_value[i], size, &fStrLen);
        break;
      case SQL_TYPE_DATE:
      case SQL_TYPE_TIME:
      case SQL_TYPE_TIMESTAMP:
      case SQL_DATETIME:
      case SQL_BIGINT:
      case SQL_DECFLOAT:
      case SQL_SMALLINT:
      case SQL_INTEGER:
      case SQL_REAL:
      case SQL_FLOAT:
      case SQL_DOUBLE:
      case SQL_DECIMAL:
      case SQL_NUMERIC:
      default:
        size = DB2UTIL_EXPAND_OTHER;
        buff_value[i] = db2util_new(size);
        rc = SQLBindCol((SQLHSTMT)hstmt, (i + 1), SQL_CHAR, buff_value[i], size, &fStrLen);
        break;
      }
      if (db2util_check_sql_errors(hstmt, SQL_HANDLE_STMT, rc) == SQL_ERROR) {
        return SQL_ERROR;
      }
    }
    rc = SQL_SUCCESS;
    while (rc == SQL_SUCCESS) {
      rc = SQLFetch(hstmt);
      if (rc == SQL_NO_DATA_FOUND) {
        break;
      }
      recs += 1;
      for (i = 0 ; i < nResultCols; i++) {
        if (buff_value[i]) {
          printf(" -%s",buff_name[i]);
          /* trim */
          strip_done = 0;
          strip_value = buff_value[i];
          len_value = strlen(strip_value);
          for (j=len_value;j;j--) {
            if (!strip_done && (strip_value[j] == ' ' || !strip_value[j])) {
              strip_value[j] = '\0';
            } else {
              strip_done = 1;
              /* new lines be gone */
              if (strip_value[j] == '\n') {
                strip_value[j] = ' ';
              }
            }
          }
          /* space means "value quotes" */
          iamspace = strchr(buff_value[i], ' ');
          if (iamspace) {
            printf(" \"%s\"",buff_value[i]);
          } else {
            printf(" %s",buff_value[i]);
          }
        }
      }
      printf("\n");
    }
    for (i = 0 ; i < nResultCols; i++) {
      if (buff_value[i]) {
        db2util_free(buff_name[i]);
        buff_name[i] = NULL;
      }
      if (buff_name[i]) {
        db2util_free(buff_name[i]);
        buff_name[i] = NULL;
      }
    }
  }
  rc = SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
  rc = SQLDisconnect((SQLHDBC)hdbc);
  rc = SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
  /* SQLFreeHandle(SQL_HANDLE_ENV, henv); */
  return rc;
}


void help(char *str) {
  printf("Syntax (Version %s):\n",SYS400_VERSION);
  printf("  system400 -q|-qsh -c|-cmd -d|-db2 command/sql [-root /path/mychroot | -? db2parm1 'db2 parm 2' ...]\n");
  printf("    -q|-qsh    - qsh system '*CMD' with *OUTPUT expected\n", str);
  printf("    -c|-cmd    - *CMD no *OUTPUT expected\n", str);
  printf("    -d|-db2    - DB2 SQL\n", str);
  printf("    -r|-root   - chroot root /path/mychroot (CHROOT)\n", str);
  printf("                 $export CHROOT=/path/mychroot\n");
  printf("   -ll|-libl   - *LIBL CHGLIBL before command execution (LIBL)\n", str);
  printf("                 $export LIBL=\"MYLIB YOURLIB BOBLIB\"\n");
  printf("   -al|-addlib - *LIBL ADDLIBLE before command execution (ADDLIB)\n", str);
  printf("                 $export ADDLIB=MYLIB\n");
  printf("   -cl|-curlib - *LIBL CHGCURLIB before command execution (CURLIB)\n", str);
  printf("                 $export CURLIB=MYLIB\n");
  printf("Syntax(alternative positional obsolete, but works):\n");
  printf("  system400 command/sql [/path/mychroot | db2parm1 'db2 parm 2' ...]\n");
}

int main(int argc, char * argv[]) {
  int ccsid = db2util_ccsid();
  int ebcdic = SYS400_EBCDIC_CCSID;
  int i = 0;
  int j = 0;
  char *a = NULL;
  char *v = NULL;
  char op = '*';
  char *cmd = NULL;
  char precmd[SYS400_MAX_CMD];
  char *root = getenv("CHROOT");
  char *addlib = getenv("ADDLIB");
  char *libl = getenv("LIBL");
  char *curlib = getenv("CURLIB");
  char *find = NULL;
  char lookcount = 4;
  char *look[] = {"select","insert","update","drop"};
  int argc1 = 0;
  int positional = 1;
  int parm_1st = 0;
  char * argv1[DB2UTIL_MAX_ARGS];
  SQLRETURN sqlrc = SQL_SUCCESS;
  SQLRETURN sql_exec_rc = SQL_SUCCESS;
  SQLHANDLE henv = 0;
  SQLHANDLE hdbc = 0;
  SQLINTEGER attr = SQL_TRUE;
  SQLINTEGER attr_isolation = SQL_TXN_NO_COMMIT;

  /* parms */
  for (i=1; i < argc; i++) {
    a = argv[i];
    v = argv[i + 1];
    /* == -key 'value' == */
    switch (a[0]) {
    case '-':
      switch (a[1]) {
      case 'a':
        addlib = v;
        break;
      case 'c':
        switch(a[2]) {
        case 'l':
          curlib = v;
          break;
        default:
          op = 'c';
          cmd = v;
        }
        break;
      case 'd':
        op = 'd';
        cmd = v;
        break;
      case 'l':
        libl = v;
        break;
      case 'q':
        op = 'q';
        cmd = v;
        break;
      case 'r':
        root = v;
        break;
      case '?':
        parm_1st = i + 1;
        break;
      default:
        break;
      }
      positional = 0;
      i++;
      break;
    /* == positional == */
    default:
      if (!positional) {
        break;
      }
      if (!cmd) {
        cmd = a;
        /* guess operation is qsh */
        op = 'q';
        /* check operation maybe db2 */
        for (j=0;j<lookcount;j++) {
          find = strstr(a,look[j]);
          if (find) {
            op = 'd';
            break;
          }
        }
        parm_1st = i;
      } else if (!root) {
        root = a;
        parm_1st = i;
      }
      break;
    }
  }

  /* help required */
  if (op == '*') {
    help("WRKJOB JOB(*) OPTION(*JOBLOG)");
    return -1;
  }

  /* environment db2 */
  sqlrc = SQLOverrideCCSID400( ccsid );
  /* need env handle */
  sqlrc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);
  /* need server mode (attach QSQ to escape chroot) */
  sqlrc = SQLSetEnvAttr((SQLHENV)henv, SQL_ATTR_SERVER_MODE, (SQLPOINTER)&attr, (SQLINTEGER) 0);
  /* need conn handle */
  sqlrc = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);
  /* system naming (select * from QIWS/QCUSTCDT) */
  sqlrc = SQLSetConnectAttr((SQLHDBC)hdbc, SQL_ATTR_DBC_SYS_NAMING, (SQLPOINTER)&attr, SQL_IS_INTEGER);
  /* enable db2 work in CRTLIB (no journal) */
  sqlrc = SQLSetConnectAttr((SQLHDBC)hdbc, SQL_ATTR_TXN_ISOLATION, (SQLPOINTER)&attr_isolation, SQL_IS_INTEGER);
  /* connection to db2 as current profile (server mode QSQSRVR job) */
  sqlrc = SQLConnect(hdbc, NULL, 0, NULL, 0, NULL, 0);
  /* LIBL? */
  if (libl) {
    memset(precmd,0,SYS400_MAX_CMD);
    sprintf(precmd,"CHGLIBL LIBL(%s)",libl);
    argc1 = 4;
    argv1[0] = argv[0];  
    argv1[1] = precmd;  
    argv1[2] = root;  
    argv1[3] = NULL;  
    sql_exec_rc = CMD_QSQSRVR(hdbc, argv1, 0);
  }
  /* ADDLIB? */
  if (addlib) {
    memset(precmd,0,SYS400_MAX_CMD);
    sprintf(precmd,"ADDLIBLE LIB(%s)",addlib);
    argc1 = 4;
    argv1[0] = argv[0];  
    argv1[1] = precmd;  
    argv1[2] = root;  
    argv1[3] = NULL;  
    sql_exec_rc = CMD_QSQSRVR(hdbc, argv1, 0);
  }
  /* CURLIB? */
  if (curlib) {
    memset(precmd,0,SYS400_MAX_CMD);
    sprintf(precmd,"CHGCURLIB CURLIB(%s)",addlib);
    argc1 = 4;
    argv1[0] = argv[0];  
    argv1[1] = precmd;  
    argv1[2] = root;  
    argv1[3] = NULL;  
    sql_exec_rc = CMD_QSQSRVR(hdbc, argv1, 0);
  }
  /* make request (output to stdout) */
  switch (op) {
  case 'q':
    argc1 = 4;
    argv1[0] = argv[0];  
    argv1[1] = cmd;  
    argv1[2] = root;  
    argv1[3] = NULL;  
    sql_exec_rc = STRQSH_QSQSRVR(hdbc, ccsid, ebcdic, argv1);
    break;
  case 'c':
    argc1 = 4;
    argv1[0] = argv[0];  
    argv1[1] = cmd;  
    argv1[2] = root;  
    argv1[3] = NULL;  
    sql_exec_rc = CMD_QSQSRVR(hdbc, argv1, 1);
    break;
  case 'd':
    argc1 = 2;
    argv1[0] = argv[0];  
    argv1[1] = cmd;  
    for (j = parm_1st; argc1 < DB2UTIL_MAX_ARGS; j++, argc1++) {  
        argv1[argc1] = argv[j];
    }
    argv1[argc1] = NULL;
    sql_exec_rc = DB2_QSQSRVR(hdbc, argv1);
    break;
  default:
    break;
  }
  /* clean up */
  sqlrc = SQLDisconnect((SQLHDBC)hdbc);
  sqlrc = SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
  return sql_exec_rc;
}


