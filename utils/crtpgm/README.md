#crtpgm

Create program. A system400 utility.

```
$ crtpgm -h
Syntax:
  crtpgm
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -p|--pgm mycool
    -l|--lib mylib (*LIBL) (2)
    -m|--mod mycool mycool2 ... (3)
    -o|--option "BNDSRVPGM(QHTTPSVR/QZSRCORE)"
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -m|--mod mycool mycool2 ...
     -m|--mod mycool.mod mycool2.mod ...
     -m|--mod mycool.any mycool2.any ...
     Any mycool, mycool.suffix is valid.
     (Makefile may use mycool.mod)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

**module.pgm** - enable Makefile processing with timestamps 
crtpgm creates a module.pgm in current directory 
on successful compile. This additional empty module.pgm file allows 
normal Makefile processing to only compile on source module.rpgle changes.

Example (inside chroot /QOpenSys/db2sock):
```
$ crtpgm -p xmlmain -l xmlservice -m xmlmain plugbug plugipc plugrun plugperf plugcach plugerr 
                       plugsql plugdb2 plugconf1 plugpase pluglic plugsig plugconv plugxml plugile

system400 -cmd 'CRTPGM PGM(xmlservice/xmlmain) MODULE( xmlservice/xmlmain xmlservice/plugbug 
  xmlservice/plugipc xmlservice/plugrun xmlservice/plugperf xmlservice/plugcach xmlservice/plugerr 
  xmlservice/plugsql xmlservice/plugdb2 xmlservice/plugconf1 xmlservice/plugpase xmlservice/pluglic 
  xmlservice/plugsig xmlservice/plugconv xmlservice/plugxml xmlservice/plugile )' 
  -r /QOpenSys/db2sock
==> xmlservice/xmlmain -- ok
```
