#crtsrcpf

Create source file. A system400 utility.

```
$ crtsrcpf -h
Syntax:
  crtsrcpf
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -l|--lib mylib (*LIBL) (2)
    -f|--file qclsrc
    -v|--verbose
    -o|--option "RCDLEN(92) MBR(*NONE) TEXT('my src')"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ crtsrcpf -f tonysrc -l xmlservice
```
