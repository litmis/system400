#crtlib

Create library. A system400 utility.

```
$ crtlib -h
Syntax:
  crtlib
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -l|--lib mylib
    -v|--verbose
    -o|--option "TEXT('my new lib') TYPE(*PROD)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ crtlib -l frogblow

bash-4.3$ dsplib -l frogblow
  5770SS1 V7R1M0  100423                   Display Library                                        6/22/17 13:04:50        Page    1
  Library . . . . . . . . . . . . . . . . :   FROGBLOW
  Type  . . . . . . . . . . . . . . . . . :   PROD
...
```
