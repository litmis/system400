#crtclpgm

Create CL program. A system400 utility.

```
$ crtclpgm -h 
Syntax:
  crtclpgm
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -s|--src mycool.clp
    -l|--lib mylib (*LIBL) (2)
   -tl|--tlib copy source library (3)
   -ts|--tsrc copy source file (3)
   -tm|--tmbr copy source file member (3)
    -o|--option "USRPRF(*USER)"
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -tl|--tlib copy source library
     -ts|--tsrc copy source file
     -tm|--tmbr copy source file member
     required copy --src to (-tl)/(-ts).(-tm)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

**module.clppgm** - enable Makefile processing with timestamps 
crtpgm creates a module.pgm in current directory 
on successful compile. This additional empty module.clppgm file allows 
normal Makefile processing to only compile on source module.rpgle changes.

Example (inside chroot /QOpenSys/db2sock):
```
$ crtclpgm -s crtxml.clp -l db2 -tl adc -ts mysrc -tm crtx

$ crtclpgm -s crtxml.clp -l db2 -tl adc -ts mysrc -tm crtx -v
crtlib -l adc
crtsrcpf -f mysrc -l adc
cpyfrmstmf -f crtxml.clp -t /qsys.lib/adc.lib/mysrc.file/crtx.mbr
system400 -cmd "CRTCLPGM PGM(db2/crtxml) SRCFILE(adc/mysrc) SRCMBR(crtx) OUTPUT(*NONE)"
-success crtxml
```
