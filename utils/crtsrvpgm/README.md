#crtsrvpgm

Create service program. A system400 utility.

```
$ crtsrvpgm -h
Syntax:
  crtsrvpgm
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -p|--pgm mycool
    -l|--lib mylib (*LIBL) (2)
    -m|--mod mycool mycool2 ... (3)
    -o|--option "EXPORT(*ALL) ACTGRP(*CALLER) BNDSRVPGM(QHTTPSVR/QZSRCORE)"
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -m|--mod mycool mycool2 ...
     -m|--mod mycool.mod mycool2.mod ...
     -m|--mod mycool.any mycool2.any ...
     Any mycool, mycool.suffix is valid.
     (Makefile may use mycool.mod
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

**module.srvpgm** - enable Makefile processing with timestamps 
crtsrvpgm creates a module.pgm in current directory 
on successful compile. This additional empty module.pgm file allows 
normal Makefile processing to only compile on source module.rpgle changes.

Example (inside chroot /QOpenSys/db2sock):
```
$ crtsrvpgm -p xmlservice -l xmlservice -m xmlservice plugbug plugipc 
     plugrun plugperf plugcach plugerr plugsql plugdb2 plugconf1 
     plugpase pluglic plugsig plugconv plugxml plugile
     -o "EXPORT(*ALL) ACTGRP(*CALLER)"
==> xmlservice/xmlservice -- ok
```
