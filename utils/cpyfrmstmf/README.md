#cpyfrmstmf

Copy from stream file. A system400 utility.

```
$ cpyfrmstmf -h                                                                                     
Syntax:
  cpyfrmstmf
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -f|--from myfile.txt (2)
    -t|--to /qsys.lib/mylib/mysrc.file/mycl.mbr
    -v|--verbose
    -o|--option "STMFCCSID(819) DBFCCSID(37)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -f|--from myfile.txt
     chroot relative (do not include CHROOT)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ cpyfrmstmf -f ../../xmlservice-rpg/crtxml.clp -t /qsys.lib/xmlservice.lib/qclsrc.file/tony8.mbr
```
