#!/bin/sh
TARGET=/QOpenSys/usr/bin
utils="crtpgm crtsrvpgm crtrpgmod crtsqlrpgi dltmod crtclpgm\
 wrkactjob wrksyssts rtvjoba\
 dsplibl dsplib crtlib dltlib\
 crtsrcpf dltf cpyfrmstmf\
 db2script"
for u in $utils
do
  echo "chmod +x $u/$u"
  chmod +x "$u/$u"
  echo "cp $u/$u $TARGET/$u"
  cp "$u/$u" "$TARGET/$u"
done


